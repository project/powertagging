<?php

namespace Drupal\powertagging\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\semantic_connector\Entity\SemanticConnectorPPServerConnection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\powertagging\Entity\PowerTaggingConfig;
use Drupal\powertagging\PowerTagging;

class PowertaggingCommands {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PowertaggingCommands object.
   *
   * @param LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->logger = $logger->get('powertagging');
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * A custom Drush command to displays the given text.
   *
   * @command powertagging:tag-content
   *
   * @param string $powertagging_config
   *   The powertagging config ID.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The bundle.
   * @param string $field
   *   The field ID.
   * @param array $options
   *   Array of options as described below.
   *
   * @option keep-existing-tags Keep the existing tags.
   * @option skip-tagged-content Skip tagged content.
   */
  public function tagContent(string $powertagging_config, string $entity_type_id, string $bundle, string $field, array $options = ['keep-existing-tags' => FALSE, 'skip-tagged-content' => FALSE]) {
    $config = PowerTaggingConfig::load($powertagging_config);
    if (empty($config)) {
      $this->messenger->addError($this->t('Unknown powertagging config @config', [
        '@config' => $powertagging_config,
      ]));
    }

    $powertagging = new PowerTagging($config);

    $field_info = [
      'entity_type_id' => $entity_type_id,
      'bundle' => $bundle,
      'field_type' => $field,
    ];

    $tag_settings = $powertagging->buildTagSettings($field_info, [
      'skip_tagged_content' => $options['skip-tagged-content'] ?? FALSE,
      'keep_existing_tags' => $options['keep-existing-tags'] ?? FALSE,
    ]);

    $bundle_key = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('bundle');
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery()
      ->condition($bundle_key, $bundle);

    if (!empty($options['skip-tagged-content'])) {
      $query->notExists($field);
    }

    $entities = $query->execute();

    if (empty($entities)) {
      $this->logger->notice($this->t('No entities to tag'));
      return;
    }

    $this->logger->notice($this->t('Started tagging @count entities', [
      '@count' => count($entities),
    ]));

    $idx = 0;

    foreach ($entities as $entity) {
      $idx++;
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity);

      if (!$entity) {
        continue;
      }

      $existing_tags = [];
      if (!empty($options['keep-existing-tags']) && $entity->hasField($field)) {
        $existing_tags = $entity->get($field)->getValue();
      }

      try {
        $tags = $powertagging->extractTagsOfEntity($entity, $tag_settings);
        $tag_ids = array_column($tags, 'target_id');
        foreach ($existing_tags as $existing_tag) {
          if (in_array($existing_tag['target_id'], $tag_ids)) {
            continue;
          }
          $tags[] = $existing_tag;
        }

        $entity->set($field, $tags);
        $entity->save();

        $this->logger->notice($this->t('Successfully tagged @entity_type @entity_id (@index/@count)', [
          '@entity_type' => $entity_type_id,
          '@entity_id' => $entity->id(),
          '@index' => $idx,
          '@count' => count($entities),
        ]));
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    $this->logger->notice($this->t('Successfully tagged @count entities.', [
      '@count' => $idx,
    ]));
  }

  /**
   * A custom Drush command to displays the given text.
   *
   * @field-labels
   *     label: Term
   *     score: Score
   *     positions: Positions
   * @command powertagging:extract-concepts
   *
   * @param string $pp_server_id PoolParty server ID configured in "Semantic Connector"
   * @param string $project_id PoolParty project UUID
   * @param string $extractor_options Additional extractor options (e.g. numberOfTerms, showMatchingDetails, showMatchingPosition, numberOfConcepts etc. ). See https://help.poolparty.biz/en/developer-guide/enterprise-server-apis/entity-extractor-apis/information-extraction-services/concept-extraction-service/web-service-method--extract-from-text.html
   * @param string $file_path Path to an existing file or plain text content between quotes
   *
   * @example ./vendor/bin/drush powertagging:extract-concepts brs_dev_server 940c5505-28f7-447b-8302-3bcfaefdb0ac categorize=0  "Surrender to the Regional Protocols"
   * @example ./vendor/bin/drush powertagging:extract-concepts brs_dev_server 940c5505-28f7-447b-8302-3bcfaefdb0ac ""  lcc-ph-act-1999-8749.html
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|int
   * @noinspection PhpDocSignatureInspection
   */
  public function extractConcepts(InputInterface $input) { // string $pp_server_id, string $project_id, array $options = [], string $file_path = '') {
    $pp_server_id = $input->getArgument('pp_server_id');
    /** @var \Drupal\semantic_connector\Entity\SemanticConnectorPPServerConnection $ppServer */
    if (!$ppServer = SemanticConnectorPPServerConnection::load($pp_server_id)) {
      $this->logger->error($this->t('Unknown PP server @config', [
        '@config' => $pp_server_id,
      ]));
      return -1;
    }
    $project_id = $input->getArgument('project_id');
    $file_path = $input->getArgument('file_path');
    $extractor_options = $input->getArgument('extractor_options');
    $default_options = [
      'projectId' => $project_id,
      'numberOfTerms' => 0,
      'showMatchingDetails' => 1,
      'showMatchingPosition' => 1,
      'numberOfConcepts' => 999,
      'displayText' => 0,
      'categorize' => 1,
    ];
    if(!empty($extractor_options)) {
      $options = explode(',', $extractor_options);
      foreach($options as $option) {
        $parts = explode('=', $option);
        if(count($parts) == 2) {
          $default_options[$parts[0]] = $parts[1];
        }
      }
    }
    $language = 'en';
    if(!empty($file_path) && is_readable($file_path)) {
      $response = $ppServer->getApi()->extractConcepts(
        (object)['file_path' => $file_path],
        $language, $default_options, 'file direct'
      );
    } else {
      $response = $ppServer->getApi()->extractConcepts(
        $file_path,
        $language, $default_options, 'text'
      );
    }
    //
    $ret = [];
    if(!empty($response['concepts'])) {
      foreach ($response['concepts'] as $concept) {
        $charIndexes = [];
        foreach ($concept['matchingLabels'] as $matchingLabels) {
          foreach ($matchingLabels['matchedTexts'] as $matchedTexts) {
            foreach ($matchedTexts['positions'] as $position) {
              $charIndexes[] = $position['beginningIndex'];
            }
          }
        }
        $ret[] = [
          'label' => $concept['prefLabels']['en'],
          'score' => $concept['score'],
          'positions' => implode(',', $charIndexes),
        ];
      }
    }
    if(!empty($response['categories'])) {
      $ret[] = [
        'label' => '--------------------------------',
        'score' => '',
        'positions' => '',
      ];
      foreach ($response['categories'] as $category) {
        $ret[] = [
          'label' => $category['prefLabel'],
          'score' => round($category['score'], 2),
          'positions' => 'n/a',
        ];
      }
    }
    return new RowsOfFields($ret);
   }
}